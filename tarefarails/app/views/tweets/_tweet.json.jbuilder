json.extract! tweet, :id, :publication, :created_at, :updated_at
json.url tweet_url(tweet, format: :json)
